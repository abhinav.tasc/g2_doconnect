import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Question } from './../question';
import { User } from '../user';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private userService:UserService) { }

  ngOnInit(): void {
  }

  questions:Question[] | undefined
  response:any
  user= new User()




  isSearched:boolean=false
  getValue(values:string){
    if(values !=='')
    this.userService.searchQuestion(values).subscribe((data)=>{
      console.log(data)
      this.questions=data
      if(data.length==0){
        alert("No Question Found")
      }else{
      this.isSearched=true}
    })

  }

}
